print('generate volume example...')
import bpy, math

for ob in bpy.data.objects:
	if ob.type != "LIGHT":
		ob.hide_set(True)

gmat = bpy.data.materials.new(name='volume')
bpy.data.materials.create_gpencil_data(gmat)
gmat.grease_pencil.show_fill = True
gmat.grease_pencil.fill_color = [0.1,0.1,0.8, 0.01]

g = bpy.data.grease_pencils.new(name='volume-data')
g.materials.append( gmat )
gob = bpy.data.objects.new(name='volume-object', object_data=g)
bpy.context.collection.objects.link(gob)


def make_circle(radius=1.0):
	layer = g.layers.new('mylayer')
	frame = layer.frames.new(1)  ## 1 is the first frame
	a = frame.strokes.new()
	a.draw_cyclic = True
	#a.line_width = 0

	a.points.add( count=360 )
	for i in range(360):
		a.points[i].co.x = math.sin( math.radians(i) ) * radius
		a.points[i].co.y = math.cos( math.radians(i) ) * radius

	return layer



for i in range(360):
	empty = bpy.data.objects.new(name=str(i), object_data=None)
	bpy.context.collection.objects.link(empty)
	lay = make_circle(radius=i*0.03)
	lay.parent = empty
	empty.rotation_euler.x = math.radians(i)

print('OK')

