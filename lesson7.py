## https://gitlab.com/hartsantler/panjeaplugin
## install blender2.8 and get the panjeaplugin, then run:
## cd panjeaplugin
## ./panjeaplugin.py lesson7.py

Mario = [
	'     RRRRR      ',
	'    RRRRRRRRR   ',
	'    HHH--0-     ',
	'   H-H---0---   ',
	'   H-HH---0---- ',
	'   HH----00000  ',
	'     --------   ',
	'  RRRRBBRRR     ',
	'--RRRRBBBRRRR---',
	'--- RRBYBBBBRR--',
	'-- BBBBBBBBBB   ',
	'  BBBBBBBBBBBB  ',
	' BBBBB    BBBB  ',
	'HHBBB      BBB  ',
	'HHHH       HHHH ',
	' HHHHH     HHHHH',
]

bpy.ops.mesh.primitive_cube_add()
cube = bpy.context.active_object

def new_material(name, color):
	mat = bpy.data.materials.new(name=name)
	mat.use_nodes = False
	mat.diffuse_color = color + [1]
	return mat

RED = new_material('red', [1,0,0])
BLUE = new_material('blue', [0,0,1])
BLACK = new_material('black', [0,0,0])
BROWN = new_material('brown', [0.3,0.2,0.2])
SKIN = new_material('skin', [0.8,0.6,0.6])

root = bpy.data.objects.new(name='root', object_data=None )
bpy.context.collection.objects.link(root)

z = len(Mario)
for line in Mario:
	z -= 1
	x = 0
	for char in line:
		x -= 1
		if char.strip():
			ob = bpy.data.objects.new(name='mario', object_data=cube.data.copy() )
			bpy.context.collection.objects.link(ob)
			if char=='R':
				ob.data.materials.append(RED)
			elif char=='B':
				ob.data.materials.append(BLUE)
			elif char=='0':
				ob.data.materials.append(BLACK)
			elif char=='H':
				ob.data.materials.append(BROWN)
			elif char=='-':
				ob.data.materials.append(SKIN)
			ob.scale *= 0.5
			ob.location.x = x
			ob.location.z = z
			ob.parent = root

root.use_ode_physics = True
root.use_ode_gamepad = True
root.gamepad_lin_force_x = -50
root.gamepad_lin_force_z = -50
bpy.context.view_layer.objects.active = root
#bpy.ops.wm.tool_set_by_id(name='builtin.cursor')
