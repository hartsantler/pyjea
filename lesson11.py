#!/usr/bin/python
# -*- coding: utf-8 -*-
## Lesson11 - Data Driven Nodes Example ##
## example  - geneate metaballs: ./lesson.py ⭖

import sys, os, json, subprocess

SYMBOL = sys.argv[-1].decode('utf-8')
assert len(SYMBOL)==1

TILE_OP_CODE = 100

Scene = {
	'id' : 1,
	"scene_width" : 64000, "scene_height": 64000, 
	"nodes": [],
	"edges": [],
}

Colors = {
	'R':[1,0,0],
	'H':[0.3,0.3,0.1],
	'B':[0,0,1],
	'Y':[1,1,0],
	'0':[0,0,0],
	'-':[0.8,0.7,0.5]
}

## Example Data ##
MyData = [
	'     RRRRR      ',
	'    RRRRRRRRR   ',
	'    HHH--0-     ',
	'   H-H---0---   ',
	'   H-HH---0---- ',
	'   HH----00000  ',
	'     --------   ',
	'  RRRRBBRRR     ',
	'--RRRRBBBRRRR---',
	'--- RRBYBBBBRR--',
	'-- BBBBBBBBBB   ',
	'  BBBBBBBBBBBB  ',
	' BBBBB    BBBB  ',
	'HHBBB      BBB  ',
	'HHHH       HHHH ',
	' HHHHH     HHHHH',
]

rtf = []
for ln in MyData:
	new_line = u''
	for char in ln:
		if char in Colors:
			r,g,b = Colors[char]
			r = int(r*255)
			g = int(g*255)
			b = int(b*255)
			new_line += u'<span style="color:rgb(%s,%s,%s)">%s</span>' %(r,g,b, SYMBOL)
		else:
			new_line += u'⟹'
	rtf.append( new_line )

tile = {
	"id": 2, 
	"title": "Tile", 
	"pos_x": 5.0, 
	"pos_y": -122.0, 
	"inputs": [],
	"outputs":[],
	"content": {
		"value": '<br/>'.join(rtf),
		"name" : 'mario.tile'
	},
	"op_code" : TILE_OP_CODE
}
Scene['nodes'].append( tile )

data = json.dumps( Scene )
open('/tmp/mario_tile.json', 'wb').write(data)

if '--blender27' in sys.argv:
	subprocess.check_call([os.path.expanduser('~/panjeaplugin/panjeaplugin.py'), '/tmp/mario_tile.json', '--blender27'])
else:
	subprocess.check_call([os.path.expanduser('~/panjeaplugin/panjeaplugin.py'), '/tmp/mario_tile.json'])
