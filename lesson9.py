#!/usr/bin/python
# -*- coding: utf-8 -*-
## Lesson9 - Data Driven Nodes Example ##

CUBE_OP_CODE = 83
INSTANCE_OP_CODE = 82

ID = 0
def genid():
	global ID
	ID += 1
	return ID

Scene = {
	'id' : genid(),
	"scene_width" : 64000, "scene_height": 64000, 
	"nodes": [],
	"edges": [],
}

Colors = {
	'R':[1,0,0],
	'H':[0.3,0.3,0.1],
	'B':[0,0,1],
	'Y':[1,1,0],
	'0':[0,0,0],
	'-':[0.8,0.7,0.5]
}

Cubes = {}
y = 0
for symbol in 'RHBY0-':
	color = Colors[ symbol ]
	cubeid = genid()
	outid  = genid()
	y += 200
	cube = {
		'id':cubeid,
		"title": "Make Cube", 
		"pos_x": -100, 
		"pos_y": y, 
		'inputs':[],
		'outputs':[
			{
				"id": outid, 
				"index": 0, "multi_edges": True, "position": 5, "socket_type": 6
			}
		],
		"content": {
			"object_name": symbol, 
			"object_clr": color, 
		}, 
		"op_code": CUBE_OP_CODE
	}
	Cubes[ symbol ] = outid
	Scene['nodes'].append( cube )

## Example Data ##
MyData = [
	'     RRRRR      ',
	'    RRRRRRRRR   ',
	'    HHH--0-     ',
	'   H-H---0---   ',
	'   H-HH---0---- ',
	'   HH----00000  ',
	'     --------   ',
	'  RRRRBBRRR     ',
	'--RRRRBBBRRRR---',
	'--- RRBYBBBBRR--',
	'-- BBBBBBBBBB   ',
	'  BBBBBBBBBBBB  ',
	' BBBBB    BBBB  ',
	'HHBBB      BBB  ',
	'HHHH       HHHH ',
	' HHHHH     HHHHH',
]

y = 0
for ln in MyData:
	x = 0
	y += 200
	for char in ln:
		x += 150
		if not char.strip():
			continue
		instance_id = genid()
		instance_input = genid()

		node = {
			"id": instance_id, 
			"title": "Instance", 
			"pos_x": x, 
			"pos_y": y, 
			"inputs": [
				{
					"id": instance_input, 
					"index": 7, "multi_edges": False, "position": 2, "socket_type": 6
				}
			], 
			"outputs": [
				{
					"id": genid(), 
					"index": 0, "multi_edges": True, "position": 5, "socket_type": 6
				}
			], 
			"content": {
				"object_name": '%s_%s' %(char,instance_id), 
			}, 
			"op_code": INSTANCE_OP_CODE
		}

		Scene['nodes'].append( node )
		edge = {
			"id": genid(), 
			"edge_type": 1, 
			"start": Cubes[char], 
			"end": instance_input
		}
		Scene['edges'].append( edge )

import sys, os, json, subprocess

data = json.dumps( Scene )
open('/tmp/mario.json', 'wb').write(data)

if '--blender27' in sys.argv:
	subprocess.check_call([os.path.expanduser('~/panjeaplugin/panjeaplugin.py'), '/tmp/mario.json', '--blender27'])
else:
	subprocess.check_call([os.path.expanduser('~/panjeaplugin/panjeaplugin.py'), '/tmp/mario.json'])
