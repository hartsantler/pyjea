import os, sys

def _find_getch():
	try:
		import termios
	except ImportError:
		# Non-POSIX. Return msvcrt's (Windows') getch.
		import msvcrt
		return msvcrt.getch
	# POSIX system. Create and return a getch that manipulates the tty.
	import sys, tty
	def _getch():
		fd = sys.stdin.fileno()
		old_settings = termios.tcgetattr(fd)
		try:
			tty.setraw(fd)
			ch = sys.stdin.read(1)
		finally:
			termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
		return ch
	return _getch

getch = _find_getch()

Mario = [
	'     RRRRR      ',
	'    RRRRRRRRR   ',
	'    HHH--0-     ',
	'   H-H---0---   ',
	'   H-HH---0---- ',
	'   HH----00000  ',
	'     --------   ',
	'  RRRRBBRRR     ',
	'--RRRRBBBRRRR---',
	'--- RRBYBBBBRR--',
	'-- BBBBBBBBBB   ',
	'  BBBBBBBBBBBB  ',
	' BBBBB    BBBB  ',
	'HHBBB      BBB  ',
	'HHHH       HHHH ',
	' HHHHH     HHHHH',
]

BLACK = '30'
RED = '31'
GREEN = '32'
YELLOW = '33'
BLUE = '34'
GRAY = '90'
WHITE = '37'

ECODE = '\033[%sm█\033[0m'

mx = 0
my = 0
def drawmario():
	os.system("clear")
	offset = ' ' * mx
	for i in range (my):
		print()

	for ln in Mario:
		ln = ln.replace( '0', ECODE % BLACK )
		ln = ln.replace( 'R', ECODE % RED )
		ln = ln.replace( 'B', ECODE % BLUE )
		ln = ln.replace( '-', ECODE % YELLOW )
		ln = ln.replace( 'H', ECODE % GRAY )
		ln = ln.replace( 'Y', ECODE % WHITE )
		print( offset + ln)

drawmario()

RUNNING = True
while True:
	a = getch()
	if a is 'q':
		RUNNING = False
		break
	elif a is 'a':
		mx -= 1
	elif a is 'd':
		mx += 1

	if a is 'w':	
		my -= 1
	elif my < 8:
		my +=1
	drawmario()

