import os, sys
from select import select

Mario = [
	'     RRRRR      ',
	'    RRRRRRRRR   ',
	'    HHH--0-     ',
	'   H-H---0---   ',
	'   H-HH---0---- ',
	'   HH----00000  ',
	'     --------   ',
	'  RRRRBBRRR     ',
	'--RRRRBBBRRRR---',
	'--- RRBYBBBBRR--',
	'-- BBBBBBBBBB   ',
	'  BBBBBBBBBBBB  ',
	' BBBBB    BBBB  ',
	'HHBBB      BBB  ',
	'HHHH       HHHH ',
	' HHHHH     HHHHH',
]

BLACK = '30'
RED = '31'
GREEN = '32'
YELLOW = '33'
BLUE = '34'
GRAY = '90'
WHITE = '37'
ECODE = '\033[%sm█\033[0m'

direction = 1
dx = 0.0
mx = 0
my = 0
def drawmario():
	os.system("clear")
	offset = ' ' * mx
	if direction is -1:
		if int(dx):
			offset +=  ' ' * int(dx)
	elif direction is 1:
		if int(dx):
			if int(dx) > len(offset):
				offset = 0
			else:
				offset = offset[ int(dx) : ]
	for i in range (my):
		print(end="\r\n")

	for ln in Mario:
		if direction == -1:
			lnr = list(ln)
			lnr.reverse()
			ln = ''.join(lnr)
		ln = ln.replace( '0', ECODE % BLACK )
		ln = ln.replace( 'R', ECODE % RED )
		ln = ln.replace( 'B', ECODE % BLUE )
		ln = ln.replace( '-', ECODE % YELLOW )
		ln = ln.replace( 'H', ECODE % GRAY )
		ln = ln.replace( 'Y', ECODE % WHITE )
		## https://stackoverflow.com/questions/12231794/python-in-raw-mode-stdin-print-adds-spaces
		print( offset + ln, end="\r\n")

drawmario()

import tty, termios
fd = sys.stdin.fileno()
old_settings = termios.tcgetattr(fd)
tty.setraw(fd)


RUNNING = True
while True:
	rlist, _, _ = select([sys.stdin], [], [], 0.07)
	if rlist:
		a = sys.stdin.read(1)		
		if a is 'q':
			RUNNING = False
			break
		elif a is 'a':
			if mx > 0:
				mx -= 3
				dx += 1.5
				direction = -1
		elif a is 'd':
			mx += 3
			dx += 1.5
			direction = 1
		elif a is 'w':	
			my -= 11
			if dx > 2:
				if direction is 1:
					mx += 12
				elif direction is -1:
					mx -= 12
	if my < 20:
		my += 2
	dx *= 0.9
	#else:
	#	dx *= 1.5


	drawmario()

termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
